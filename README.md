# Vision Bot
This Bot is a longterm project for utility and administration on
the Programmers Palace Server.

## Setup
**DO NOT CLONE THIS REPO DIRECTLY**

In order to localhost this app, you need to checkout the
[parent repository](https://gitlab.com/programmers-vision/infrastructure) 
in order to prepare a local development infrastructure!
After having done that - before `terraform apply`ing the changes, 
double-check that the variables related to Discord & this Discord Bot 
are set-up properly!

Please, also check whether you have enabled all 3 priviledged intents
on your Discord bot on the developers page, where you created your
Bot and have invited into the guild you specified in the Terraform
variables.

Having done that you can run `terraform apply` in the parent project,
which will start a dockerized environment for the Bot.

After a short bit you should see your instance of this bot online.
During development the bot interactions will only be available on the 
guild you specified in the parent project! Once you deploy this
application in production, give the API about an hour of time to
propagate the changes - after that time you should be able to see
all your slash-commands across all server!

If something is wrong with the bot, you should check the bot's logs,
which are available on 
[a properly configured Grafana Dashboard](http://metrics.localhost)
inside your development environment! For a "how-to" check the parent
repositories 
[Wiki-Page](https://gitlab.com/programmers-vision/infrastructure/-/wikis/Initial-Grafana-&-Loki-Set-Up).
