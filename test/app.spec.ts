import * as YAMl from 'yaml';
import * as fs from 'fs';

it('should always pass', () => {
  expect(true).toBeTruthy();
});

it('should compare command data', () => {
  const jsonFile = fs.readFileSync('res/commands.json', 'utf-8');
  const json = JSON.parse(jsonFile);

  const yamlFile = fs.readFileSync('res/commands.yaml', 'utf-8');
  const yaml = YAMl.parse(yamlFile);

  expect(json).toStrictEqual(yaml);
});

describe('checks command -> file correspondance', () => {
  const yamlFile = fs.readFileSync('res/commands.yaml', 'utf-8');
  const yaml: any[] = YAMl.parse(yamlFile);

  yaml.forEach((cg) => {
    if (cg.type && cg.type === 1) {
      test(`${cg.name} has a command file.`, () => {
        const path = `src/model/interaction/command/${cg.name}.interaction.ts`;
        const exists = fs.existsSync(path);
        expect(exists).toBeTruthy();
      });
    } else if (cg.options) {
      cg.options.forEach((c) => {
        if (c.type && c.type === 1) {
          test(`${cg.name}/${c.name} has a command file.`, () => {
            const path = `src/model/interaction/command/${cg.name}/${c.name}.interaction.ts`;
            const exists = fs.existsSync(path);
            expect(exists).toBeTruthy();
          });
        }
      });
    }
  });
});

describe('checks file -> command correspondance', () => {
  const yamlFile = fs.readFileSync('res/commands.yaml', 'utf-8');
  const yaml: any[] = YAMl.parse(yamlFile);
  const commands = yaml
    .map((item) => {
      if (item.type && item.type === 1) {
        return item;
      } else {
        item.options.forEach((i) => {
          i.name = `${item.name}/${i.name}`;
        });
        return item.options;
      }
    })
    .flat()
    .map((cmd) => cmd.name);

  const path = 'src/model/interaction/command';
  const topLevel = fs.readdirSync(path);

  topLevel.forEach((file) => {
    const stat = fs.lstatSync(`${path}/${file}`);
    if (stat.isDirectory()) {
      const second = fs.readdirSync(`${path}/${file}`);
      second.forEach((cmd) => {
        const cstat = fs.lstatSync(`${path}/${file}/${cmd}`);
        if (cstat.isFile()) {
          const cmdName = cmd.split('.').shift()!;
          test(`${file}/${cmdName} has a command definition.`, () => {
            expect(commands.includes(`${file}/${cmdName}`)).toBeTruthy();
          });
        }
      });
    } else if (stat.isFile()) {
      const cmdName = file.split('.').shift()!;
      test(`${cmdName} has a command definition.`, () => {
        expect(commands.includes(cmdName)).toBeTruthy();
      });
    }
  });
});

it('serves for debugging purposes', () => {
  const num: string = [1, 3] as unknown as string;
  console.log(num + 1);
});
