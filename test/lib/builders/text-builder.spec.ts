import { TextBuilder } from '@/lib';

describe('Text Builder Tests', () => {
  test('builds text as expected', () => {
    let builder: TextBuilder = new TextBuilder();
    builder.addField('key', 'value');
    expect(builder.build()).toBe('**key:** `value`\n');
    builder.addField('header');
    expect(builder.build()).toBe('**key:** `value`\n**header:**\n');
    builder.addItemField('nested-item', 'nested-value');
    expect(builder.build()).toBe(
      '**key:** `value`\n**header:**\n❧ **nested-item:** `nested-value`\n'
    );
  });
});
