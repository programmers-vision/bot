import { CommandExecutable } from '@/lib';
import { Permission } from '#/bot';
import { CommandInteraction, TextChannel } from 'discord.js';
import { logger } from '@/log';
import { Database } from '@/service';
import { Label, Log } from '@/log';
import { reply } from '@/util/discord.util';

class ConfigCommand implements CommandExecutable {
  permission: Permission = Permission.SENIOR_ADMINISTRATOR;

  async execute(interaction: CommandInteraction): Promise<void> {
    if (!interaction.guild) {
      return;
    }

    const guild = interaction.guild;
    const option = interaction.options.getString('option', true);
    const value = interaction.options.getString('value', true);

    if (option === 'log_channel') {
      if (/\d{18}/.test(value)) {
        let channel = guild.channels.cache.get(value);
        if (!channel) {
          let fetched = await guild.channels.fetch(value);
          if (!fetched) {
            logger.debug({
              message: `Could not find channel '${value}' in guild '${guild.name}'`,
              labels: Label(
                { type: Log.COMMAND, args: [interaction] },
                { target_channel_id: value }
              ),
            });
            return reply(interaction, {
              content: 'Channel Not Found!',
              ephemeral: true,
            });
          }
          channel = fetched;
        }

        if (!(channel instanceof TextChannel)) {
          logger.debug({
            message: `Channel '${channel.name}' in guild '${guild.name}' is not a text channel`,
            labels: Label(
              { type: Log.COMMAND, args: [interaction] },
              { target_channel_id: value }
            ),
          });
          return await reply(interaction, {
            content: 'Log-Channel has to be of type: `TextChannel`!',
            ephemeral: true,
          });
        }

        await Database.setupLogChannel(guild.id, channel.id);
        logger.debug({
          message: `Set log-channel to '${channel.name}' in guild '${guild.name}'`,
          labels: Label(
            { type: Log.COMMAND, args: [interaction] },
            { target_channel_id: value }
          ),
        });

        await reply(interaction, {
          content: 'Successfully setup the Log-Channel!',
        });
        await channel.send({
          content: 'Setup Log Channel @here',
        });
        return;
      } else {
        logger.debug({
          message: `Could not find channel '${value}' in guild '${guild.name}'`,
          labels: Label(
            { type: Log.COMMAND, args: [interaction] },
            { target_channel_id: value }
          ),
        });
        return reply(interaction, {
          content: 'Supplied Value is not a valid Channel ID!',
          ephemeral: true,
        });
      }
    } else {
      logger.error({
        message: `Unknown option '${option}' for config command in guild '${guild.name}'`,
        labels: Label(
          { type: Log.COMMAND, args: [interaction] },
          { target_channel_id: value }
        ),
      });
      return reply(interaction, {
        content:
          'Unsupported Configuration Option! Please Report to the Bot Developer!',
        ephemeral: true,
      });
    }
  }
}

const instance = new ConfigCommand();
export default instance;
