import { CommandExecutable } from '@/lib';
import { Permission } from '#/bot';
import { CommandInteraction } from 'discord.js';
import { logger } from '@/log';
import { Database } from '@/service';
import { Label, Log } from '@/log';
import { reply } from '@/util/discord.util';

class WarningRemoveCommand implements CommandExecutable {
  permission: Permission = Permission.MODERATOR;

  async execute(interaction: CommandInteraction): Promise<void> {
    const guild = interaction.guildId ?? interaction.user.id;
    const name = interaction.guild
      ? interaction.guild.name
      : interaction.user.tag;
    const user = interaction.options.getUser('user', true);
    const index = interaction.options.getInteger('number', true);

    try {
      const warnings = await Database.fetchWarnings(guild, user.id);
      const warning = warnings.find((w) => w.wid === index);

      if (warning) {
        await Database.revokeWarning(guild, user.id, index);

        logger.debug({
          message: `Revoked warning #${index} for '${user.tag}' in '${name}' from the database.`,
          labels: Label(
            { type: Log.COMMAND, args: [interaction] },
            { target_user_id: user.id }
          ),
        });
        return await reply(interaction, {
          content: `✅ Removed Warning #${index}`,
          ephemeral: true,
        });
      } else {
        logger.error({
          message: `The warning #${index} could not be found for user '${user.tag}' in guild '${name}'`,
          labels: Label(
            { type: Log.COMMAND, args: [interaction] },
            { target_user_id: user.id }
          ),
        });
      }
    } catch (err) {
      logger.error({
        message: `The warning #${index} could not be removed from user '${user.tag}' in guild '${name}'\n${err}`,
        labels: Label(
          { type: Log.COMMAND, args: [interaction] },
          { target_user_id: user.id }
        ),
      });
    }

    await reply(interaction, {
      content: '❌ Failed to remove warning!',
      ephemeral: true,
    });
  }
}

const instance = new WarningRemoveCommand();
export default instance;
