import { CommandExecutable } from '@/lib';
import { Permission } from '#/bot';
import { CommandInteraction } from 'discord.js';
import { Label, Log, logger } from '@/log';
import {
  comparePermissions,
  fromRole,
  PermissionIndex,
} from '@/util/perm.util';
import { Database } from '@/service';
import { reply } from '@/util/discord.util';

class RoleRemoveCommand implements CommandExecutable {
  permission: Permission = Permission.USER;
  privilegedPermission: Permission = Permission.ADMINISTRATOR;

  async execute(interaction: CommandInteraction): Promise<void> {
    if (!interaction.guild) {
      return;
    }

    try {
      let gid = interaction.guildId!;
      let user = interaction.user.id;

      let prismaUser = await Database.findOrCreateMember(
        interaction.user,
        interaction.guild
      );
      if (!prismaUser) {
        logger.error({
          message: `The user '${interaction.user.tag}' was not found in guild '${interaction.guild.name}'`,
          labels: Label(
            { type: Log.COMMAND, args: [interaction] },
            { target_user_id: interaction.user.id }
          ),
        });
        return await reply(interaction, {
          content: 'Fatal Error! Please report to the developers!',
        });
      }

      let role = interaction.options.getRole('role', true);
      let prismaRole = await Database.findOrCreateRole(gid, role.id);
      let target =
        interaction.options.getUser('user', false) ?? interaction.user;

      let privileged = target.id !== user || !prismaRole.self;
      if (privileged) {
        let minRole =
          PermissionIndex[
            Math.max(
              PermissionIndex.indexOf(fromRole(role.name)),
              PermissionIndex.indexOf(this.privilegedPermission)
            )
          ];

        if (!comparePermissions(prismaUser.permission, minRole)) {
          logger.debug({
            message: `The user '${interaction.user.tag}' had insufficient permissions to remove the role '${role.name}' from the user '${target.tag}'`,
            labels: Label(
              { type: Log.COMMAND, args: [interaction] },
              { target_user_id: target.id }
            ),
          });
          return await reply(interaction, {
            content: `Insufficient Permissions to remove the role <@&${role.id}>!`,
          });
        }
      }

      try {
        let member = interaction.guild.members.cache.get(target.id);
        if (!member) {
          member = await interaction.guild.members.fetch(target.id);
        }
        await member.roles.remove(role.id);
        await Database.disconnectRole(gid, member.id, prismaRole.id);
        logger.debug({
          message: `The user '${interaction.user.tag}' removed the role '${role.name}' from the user '${target.tag}'`,
          labels: Label(
            { type: Log.COMMAND, args: [interaction] },
            { target_user_id: target.id }
          ),
        });
        return await reply(interaction, {
          content: `Removed <@&${role.id}> from <@!${member.id}>!`,
        });
      } catch (err) {
        logger.error({
          message: `Failed to remove role '${role.name}' from '${target.tag}'.\n${err}`,
          labels: Label(
            { type: Log.COMMAND, args: [interaction] },
            { target_user_id: target.id }
          ),
        });
        return await reply(interaction, {
          content: '❌ Failed to remove role!',
          ephemeral: true,
        });
      }
    } catch (err) {
      logger.error({
        message: `Failed to connect to the database.\n${err}`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return await reply(interaction, {
        content: '❌ Failed to access database!',
        ephemeral: true,
      });
    }
  }
}

const instance = new RoleRemoveCommand();
export default instance;
