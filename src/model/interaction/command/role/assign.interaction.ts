import { CommandExecutable } from '@/lib';
import { Permission } from '#/bot';
import { CommandInteraction } from 'discord.js';
import { Label, Log, logger } from '@/log';
import {
  comparePermissions,
  fromRole,
  PermissionIndex,
} from '@/util/perm.util';
import { Database } from '@/service';
import { memberFromID, reply } from '@/util/discord.util';

class RoleAssignCommand implements CommandExecutable {
  permission: Permission = Permission.USER;
  privilegedPermission: Permission = Permission.ADMINISTRATOR;

  async execute(interaction: CommandInteraction): Promise<void> {
    if (!interaction.inGuild()) {
      return;
    }

    try {
      let gid = interaction.guildId!;
      let user = interaction.user.id;
      let role = interaction.options.getRole('role', true);

      let prismaUser = await Database.findOrCreateMember(
        interaction.user,
        interaction.guild
      );
      let prismaRole = await Database.findOrCreateRole(gid, role.id);

      let target =
        interaction.options.getUser('user', false) ?? interaction.user;

      let privileged = target.id !== user || !prismaRole.self;
      if (privileged) {
        let minRole =
          PermissionIndex[
            Math.max(
              PermissionIndex.indexOf(fromRole(role.name)),
              PermissionIndex.indexOf(this.privilegedPermission)
            )
          ];

        if (!comparePermissions(prismaUser.permission, minRole)) {
          logger.error({
            message: `Failed to find member '${target.tag}' in guild '${
              interaction.guild!.name
            }'`,
            labels: Label(
              { type: Log.COMMAND, args: [interaction] },
              { target_user_id: target.id }
            ),
          });
          return await reply(
            interaction,
            `Insufficient permissions to assign the role <@&${role.id}> to <@${target.id}>`
          );
        }
      }

      try {
        let member = await memberFromID(interaction.guild!, target.id);
        if (member === null) {
          const message = `Failed to find member '${target.tag}' in guild '${
            interaction.guild!.name
          }'`;
          logger.error({
            message,
            labels: Label(
              { type: Log.COMMAND, args: [interaction] },
              { target_user_id: target.id }
            ),
          });
          return await reply(interaction, {
            content: `Failed to find member <@${target.id}>`,
            ephemeral: true,
          });
        }

        await member.roles.add(role.id);
        await Database.assignRole(gid, target.id, prismaRole.id);
        const message = `Assigned role '${role.name}' to '${
          target.tag
        }' in guild ${interaction.guild!.name}`;
        logger.debug({
          message,
          labels: Label(
            { type: Log.COMMAND, args: [interaction] },
            { target_user_id: target.id }
          ),
        });
        return await reply(interaction, {
          content: `Assigned <@&${role.id}> to <@!${member.id}>!`,
        });
      } catch (err) {
        logger.error({
          message: `Failed to assign role '${role.name}' to '${target.tag}'.\n${err}`,
          labels: Label(
            { type: Log.COMMAND, args: [interaction] },
            { target_user_id: target.id }
          ),
        });
        return await reply(interaction, {
          content: '❌ Failed to add role!',
          ephemeral: true,
        });
      }
    } catch (err) {
      logger.error({
        message: `Failed to connect to database!\n${err}`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return await reply(interaction, {
        content: '❌ Failed to access database!',
        ephemeral: true,
      });
    }
  }
}

const instance = new RoleAssignCommand();
export default instance;
