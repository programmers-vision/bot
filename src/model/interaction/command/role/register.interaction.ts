import { CommandExecutable } from '@/lib';
import { CommandInteraction } from 'discord.js';
import { Permission } from '#/bot';
import { Label, Log, logger } from '@/log';
import { Database } from '@/service';
import { reply } from '@/util/discord.util';

class RoleRegisterCommand implements CommandExecutable {
  permission: Permission = Permission.MODERATOR;
  bannedWords: string[] = [
    'admin',
    'moderator',
    'owner',
    'bot',
    'booster',
    'teacher',
    'professor',
  ];

  async execute(interaction: CommandInteraction): Promise<void> {
    const gid = interaction.guildId ?? interaction.user.id;
    const role = interaction.options.getRole('role', true);
    let self = interaction.options.getBoolean('assignable', false);
    if (self === null) {
      self = false;
    }

    let rule = this.bannedWords.map((word) =>
      role.name.toLowerCase().includes(word)
    );
    let match = rule.findIndex((v) => v);
    if (self && match !== -1) {
      let content =
        `This role cannot be a self-role based on the exclusion rule: \`${this.bannedWords[match]}\`.\n` +
        `If you require this functionality, please ask the developer for a fix!`;
      logger.debug({
        message: `Attempting to register banned role '${role.name}' based on the trigger: '${this.bannedWords[match]}'`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return await reply(interaction, content);
    }

    try {
      await Database.findOrCreateRole(gid, role.id, self);
      let content = `Successfully registered <@&${role.id}>`;
      content += self ? ' as a self role.' : ' as an internal role.';
      logger.debug({
        message: `Registered role '${role.name}' as an internal role.`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return await reply(interaction, { content });
    } catch (err) {
      logger.error({
        message: `Failed to register role '${role.name}'.\n${err}`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return await reply(interaction, {
        content: '❌ Failed to register Role!',
        ephemeral: true,
      });
    }
  }
}

const instance = new RoleRegisterCommand();
export default instance;
