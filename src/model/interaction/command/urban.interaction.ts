import { API_URL, WordResult, WordMeaning } from '@/lib/types/urban.types';
import { CommandInteraction, MessageEmbed } from 'discord.js';
import axios, { AxiosResponse } from 'axios';
import { CommandExecutable } from '@/lib';
import { Permission } from '#/bot';
import { Label, Log, logger } from '@/log';
import { reply } from '@/util/discord.util';

class UrbanCommand implements CommandExecutable {
  permission: Permission = Permission.USER;

  static async error(interaction: CommandInteraction, word: string) {
    try {
      logger.debug({
        message: `Could not find any definitions for '${word}'`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return await reply(interaction, {
        embeds: [
          new MessageEmbed()
            .setTitle(`${word} not found!`)
            .setColor('DARK_RED')
            .setDescription('`¯\\_(ツ)_/¯`'),
        ],
      });
    } catch (err) {
      logger.error({
        message: `Interaction timed out!`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
    }
  }

  async execute(interaction: CommandInteraction): Promise<void> {
    const word = interaction.options.getString('word', true);

    let res: AxiosResponse<WordResult>;
    try {
      res = await axios.get(`${API_URL}${word}`);
    } catch (err) {
      logger.error({
        message: `HTTP/Connection Error to ${API_URL}!\n${err}`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return await UrbanCommand.error(interaction, word);
    }

    if (res.status === 404) {
      return await UrbanCommand.error(interaction, word);
    } else {
      const definitions: WordResult = res.data;
      const definition: WordMeaning = definitions.list.shift()!;
      if (definition) {
        let description =
          `📕 Meaning:\n${definition.definition}\n` +
          `🗒️ Example:\n${definition.example}\n`;

        // Description Escaping & Fixing
        description = description.replace(/[`_~|*]/g, (m) => `\\${m}`);
        if (description.length > 2000) {
          description = description.substr(0, 1997) + '...';
        }

        const embed: MessageEmbed = new MessageEmbed()
          .setTitle(`Definition: ${word}`)
          .setDescription(description)
          .setColor('BLURPLE')
          .setFooter({
            text: `provided by ${definition.author} at ${definition.written_on}`,
          })
          .setURL(definition.permalink);
        try {
          logger.debug({
            message: `Found Urban Dictionary definition for ${word}`,
            labels: Label({ type: Log.COMMAND, args: [interaction] }),
          });
          await reply(interaction, { embeds: [embed] });
        } catch (err) {
          logger.error({
            message: `Interaction timed out!`,
            labels: Label({ type: Log.COMMAND, args: [interaction] }),
          });
        }
      } else {
        return await UrbanCommand.error(interaction, word);
      }
    }
  }
}

const instance = new UrbanCommand();
export default instance;
