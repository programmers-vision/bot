import { CommandExecutable } from '@/lib';
import { Permission } from '#/bot';
import { CommandInteraction, User } from 'discord.js';
import { default as dayjs } from 'dayjs';
import { Label, Log, logger } from '@/log';
import { Database } from '@/service';
import { reply } from '@/util/discord.util';

class BanCommand implements CommandExecutable {
  permission: Permission = Permission.ADMINISTRATOR;

  static async messageBannedUser(interaction:CommandInteraction, user:User, reason:string | null){
    await user.createDM().then((chan) =>
    chan.send({
      content:
        `You have been banned for following reason:\n` +
        reason +
        `\n\nPlease contact <@299210434467069963> with any questions/concerns regarding your ban.`,
    })
  );
  await interaction.channel?.send(`📧 Successfully messaged ${user.tag} with ban reason.`)
  }

  async execute(interaction: CommandInteraction): Promise<void> {
    if (!interaction.inGuild()) {
      return;
    }

    const guild = interaction.guild!;
    const user = interaction.options.getUser('user', true);
    const reason: string = (
      interaction.options.getString('reason', false) ?? '(no reason supplied)'
    ).replace(/\\n/gi, '\n');
    const time: number | null = interaction.options.getInteger('time', false);
    
    await Database.findOrCreateMember(user, guild);
    await reply(interaction, '...', 'defer');

    let options = { days: 1 };
    if (reason) {
      Object.assign(options, { reason });
    }
    
    try {
      await BanCommand.messageBannedUser(interaction, user, reason);     
    } catch (err) {
      logger.error({
        message: `Could not message '${user.tag}' with ${reason}'\n${err}`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }, { target_user_id: user.id }),
      });
      await interaction.channel?.send(`⚠️ Could not message ${user.tag} with ban reason.`);
    }

    try {
      await guild.members.ban(user, options);
    } catch (err) {
      logger.error({
        message: `Could not ban '${user.tag}' in guild '${guild.name}'\n${err}`,
        labels: Label(
          { type: Log.COMMAND, args: [interaction] },
          { target_user_id: user.id }
        ),
      });
      await reply(
        interaction,
        {
          content: `❌ Could not ban ${user.tag}!`,
        },
        'edit'
      );
      return;
    }

    let date = dayjs();
    let timeText: string;
    if (time) {
      date = date.add(time, 'day');
      timeText = ` for ${time} days`;
    } else {
      date = date.add(10, 'year');
      timeText = ' permanently';
    }

    try {
      await Database.createBan(
        guild.id,
        user.id,
        reason ?? '*omitted*',
        date.toDate()
      );
    } catch (err) {
      logger.error({
        message: `Could not log ban of '${user.tag}' in guild '${guild.name}'\n${err}`,
        labels: Label(
          { type: Log.COMMAND, args: [interaction] },
          { target_user_id: user.id }
        ),
      });
      await reply(
        interaction,
        {
          content: `✅ Successfully banned ${user.tag}!\n❌ Ban could not be logged.`,
        },
        'edit'
      );
      return;
    }

    logger.debug({
      message: `Banned ${user.tag} in guild '${guild.name}'${timeText}`,
      labels: Label(
        { type: Log.COMMAND, args: [interaction] },
        { target_user_id: user.id }
      ),
    });
    await reply(
      interaction,
      {
        content: `✅ ${user.tag} has been banned${timeText}!`,
      },
      'edit'
    );
  }
}

const instance = new BanCommand();
export default instance;
