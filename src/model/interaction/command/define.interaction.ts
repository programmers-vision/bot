import {
  API_URL,
  WordResult,
  WordDefinition,
  WordMeaning,
} from '@/lib/types/dictapi.types';
import { CommandExecutable } from '@/lib';
import { CommandInteraction, MessageEmbed } from 'discord.js';
import axios, { AxiosResponse } from 'axios';
import { Permission } from '#/bot';
import { Label, Log, logger } from '@/log';
import { reply } from '@/util/discord.util';

class WordDefinitionCommand implements CommandExecutable {
  permission: Permission = Permission.USER;

  static async error(interaction: CommandInteraction, word: string) {
    try {
      logger.debug({
        message: `Could not find any definitions for '${word}'`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return await reply(interaction, {
        embeds: [
          new MessageEmbed()
            .setTitle(`${word} not found!`)
            .setColor('DARK_RED')
            .setDescription('`¯\\_(ツ)_/¯`'),
        ],
      });
    } catch (err) {
      logger.error({
        message: `Interaction Reply - Time Out!`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
    }
  }

  async execute(interaction: CommandInteraction): Promise<void> {
    const word = interaction.options.getString('word', true);

    let res: AxiosResponse<Array<WordResult>, any>;
    try {
      res = await axios.get(API_URL + word);
    } catch (err) {
      logger.error({
        message: `HTTP/Connection Error to ${API_URL}!\n${err}`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return WordDefinitionCommand.error(interaction, word);
    }

    if (res.status === 404) {
      return WordDefinitionCommand.error(interaction, word);
    } else {
      const detailed = interaction.options.get('detailed') ?? false;
      const definitions: Array<WordResult> = res.data;
      const definition = definitions.shift();
      if (definition) {
        const meaning: WordMeaning = definition.meanings.shift()!;
        const explication: WordDefinition = meaning.definitions.shift()!;
        let description = `📕 Meaning:\n${explication.definition}\n`;

        if (detailed) {
          description =
            `🎶 Phonetic: '${definition.phonetic}'\n` +
            `🗣️ Part of Speech: ${meaning.partOfSpeech}\n` +
            description;
          if (explication.example) {
            description += `🗒️ Example:\n${explication.example}\n`;
          }
          if (explication.synonyms.length > 0) {
            description +=
              '⬆️ Synonyms: ' + explication.synonyms.join(', ') + '\n';
          }
          if (explication.antonyms.length > 0) {
            description += `🔄 Antonyms: ${explication.antonyms.join(', ')}\n`;
          }
        }

        const embed: MessageEmbed = new MessageEmbed()
          .setTitle(`Definition: ${word}`)
          .setColor('BLURPLE')
          .setDescription(description);
        try {
          logger.debug({
            message: `Found definition for '${word}'`,
            labels: Label({ type: Log.COMMAND, args: [interaction] }),
          });
          return await reply(interaction, { embeds: [embed] });
        } catch (err) {
          logger.error({
            message: `The search for '${word}' took too long!`,
            labels: Label({ type: Log.COMMAND, args: [interaction] }),
          });
        }
      } else {
        return WordDefinitionCommand.error(interaction, word);
      }
    }
  }
}

const instance = new WordDefinitionCommand();
export default instance;
