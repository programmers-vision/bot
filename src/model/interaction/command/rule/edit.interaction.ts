import { CommandExecutable } from '@/lib';
import { CommandInteraction } from 'discord.js';
import { Label, Log, logger } from '@/log';
import { Permission } from '#/bot';
import { Database } from '@/service';
import { reply } from '@/util/discord.util';

class RuleEditCommand implements CommandExecutable {
  permission: Permission = Permission.SENIOR_ADMINISTRATOR;

  static async error(interaction: CommandInteraction, n: number) {
    await reply(interaction, {
      content: `❌ Failed to alter Rule #${n}!`,
      ephemeral: true,
    });
  }

  async execute(interaction: CommandInteraction): Promise<void> {
    const gid = interaction.guildId ?? interaction.user.id;
    const guildName = interaction.guild
      ? interaction.guild.name
      : interaction.user.tag;
    const text = interaction.options
      .getString('rule', true)
      .replace(/\\n/gi, '\n');
    const n = interaction.options.getInteger('number', true);

    try {
      const guild = await Database.findOrCreateGuild(gid);
      if (!guild || n - 1 >= guild.rules.length || n < 1) {
        logger.error({
          message: `Failed to alter Rule #${n} in guild '${guildName}'.`,
          labels: Label({ type: Log.COMMAND, args: [interaction] }),
        });
        return await RuleEditCommand.error(interaction, n);
      } else {
        const old = guild.rules[n - 1];
        guild.rules[n - 1] = text;
        await Database.setRules(gid, guild.rules);
        logger.debug({
          message: `Successfully altered Rule #${n} in guild '${guildName}'.\n- ${old}\n+ ${text}`,
          labels: Label({ type: Log.COMMAND, args: [interaction] }),
        });
        await reply(interaction, {
          content: `✅ Successfully edited Rule #${n}!`,
          ephemeral: true,
        });
      }
    } catch (err) {
      logger.error({
        message: `The rule #${n} could not be edited in guild '${guildName}'\n${err}`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      await RuleEditCommand.error(interaction, n);
    }
  }
}

const instance = new RuleEditCommand();
export default instance;
