import { CommandExecutable } from '@/lib';
import { CommandInteraction } from 'discord.js';
import { Permission } from '#/bot';
import { Database } from '@/service';
import { Label, Log, logger } from '@/log';
import { reply } from '@/util/discord.util';

class RuleDeleteCommand implements CommandExecutable {
  permission: Permission = Permission.SENIOR_ADMINISTRATOR;

  async execute(interaction: CommandInteraction): Promise<void> {
    const gid = interaction.guildId ?? interaction.user.id;
    const guildName = interaction.guild
      ? interaction.guild.name
      : interaction.user.tag;
    const n = interaction.options.getInteger('number', true);
    const guild = await Database.findOrCreateGuild(gid, !!interaction.guild);

    if (guild && guild.rules.length >= n && n > 0) {
      const rules = guild.rules.filter((_, i) => i !== n - 1);
      await Database.setRules(guild.id, rules);
      logger.debug({
        message: `Deleted rule #${n} from guild '${guildName}'.\n${
          guild.rules[n - 1]
        }`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return await reply(interaction, {
        content: `✅ Successfully deleted Rule #${n}.`,
        ephemeral: true,
      });
    } else {
      logger.error({
        message: `Failed to delete Rule #${n} from guild '${guildName}'.`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return await reply(interaction, {
        content: `❌ Failed to delete Rule #${n}!`,
        ephemeral: true,
      });
    }
  }
}

const instance = new RuleDeleteCommand();
export default instance;
