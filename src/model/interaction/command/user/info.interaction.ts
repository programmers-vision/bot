import { CommandExecutable, TextBuilder } from '@/lib';
import { Permission } from '#/bot';
import { CommandInteraction, MessageEmbed, User } from 'discord.js';
import { Label, Log, logger } from '@/log';
import { reply } from '@/util/discord.util';

class InfoInteraction implements CommandExecutable {
  permission: Permission = Permission.MODERATOR;

  async execute(interaction: CommandInteraction) {
    const user: User = interaction.options.getUser('user', true);

    // Account Type
    let accType: string;

    if (user.system) {
      accType = 'System';
    } else if (user.bot) {
      accType = 'Bot';
    } else {
      accType = 'Human';
    }

    let createdTime = user.createdAt.toLocaleString();

    let desc: string = new TextBuilder()
      .addField('Username', user.username)
      .addField('Discriminator', user.discriminator)
      .addField('ID', user.id)
      .addField('Account Type', accType)
      .addField('Joined Discord')
      .addItemField('Since', createdTime)
      .build();

    const embed: MessageEmbed = new MessageEmbed()
      .setTitle(`User Info: ${user.tag}`)
      .setColor('BLURPLE')
      .setDescription(desc)
      .setImage(user.displayAvatarURL({ dynamic: true, size: 512 }));

    if (user.dmChannel) {
      embed.setURL(`https://discord.com/channels/@me/${user.dmChannel.id}`);
    }

    logger.debug({
      message: 'Sending user info embed',
      labels: Label({ type: Log.COMMAND, args: [interaction] }),
    });
    await reply(interaction, { embeds: [embed] });
  }
}

const instance = new InfoInteraction();
export default instance;
