import { CommandExecutable } from '@/lib';
import { Permission } from '#/bot';
import { CommandInteraction } from 'discord.js';
import { Label, Log, logger } from '@/log';
import { Database } from '@/service';
import { reply } from '@/util/discord.util';

class UnbanCommand implements CommandExecutable {
  permission: Permission = Permission.ADMINISTRATOR;

  async execute(interaction: CommandInteraction): Promise<void> {
    if (!interaction.guild) {
      return;
    }

    const guild = interaction.guild;
    const user = interaction.options.getUser('user', true);

    await reply(interaction, `Unbanning ${user.tag}...`, 'defer');
    try {
      await guild.members.unban(user);
    } catch (err) {
      logger.error({
        message: `The user '${user.tag}' could not be unbanned from guild '${guild.name}'\n${err}`,
        labels: Label(
          { type: Log.COMMAND, args: [interaction] },
          { target_user_id: user.id }
        ),
      });
      await reply(
        interaction,
        {
          content: `❌ Couldn\'t pardon ${user.tag}!`,
        },
        'edit'
      );
      return;
    }

    try {
      await Database.revokeBans(guild.id, user.id);
    } catch (err) {
      logger.error({
        message: `The unban action for '${user.tag}' at '${guild.name}' could not be logged into the database!\n${err}`,
        labels: Label(
          { type: Log.COMMAND, args: [interaction] },
          { target_user_id: user.id }
        ),
      });
      await reply(
        interaction,
        {
          content: `✅ Successfully unbanned ${user.tag}!\n❌ Pardon could not be logged.`,
        },
        'edit'
      );
      return;
    }

    logger.debug({
      message: `The unban action for '${user.tag}' at '${guild.name}' was logged into the database!`,
      labels: Label(
        { type: Log.COMMAND, args: [interaction] },
        { target_user_id: user.id }
      ),
    });
    await reply(
      interaction,
      {
        content: `✅ ${user.tag} has been pardonned!`,
      },
      'edit'
    );
  }
}

const instance = new UnbanCommand();
export default instance;
