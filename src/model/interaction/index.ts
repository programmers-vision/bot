import PingInteraction from './command/ping.interaction';
import DefineInteraction from './command/define.interaction';
import UrbanInteraction from './command/urban.interaction';
import CodeBlockInteraction from './command/codeblocks.interaction';

import RuleShowInteraction from './command/rule/show.interaction';
import RuleCreateInteraction from './command/rule/create.interaction';
import RuleEditInteraction from './command/rule/edit.interaction';
import RuleDeleteInteraction from './command/rule/delete.interaction';

import RoleAssignInteraction from './command/role/assign.interaction';
import RoleDeleteInteraction from './command/role/delete.interaction';
import RoleRegisterInteraction from './command/role/register.interaction';
import RoleRemoveInteraction from './command/role/remove.interaction';
import RoleShowInteraction from './command/role/show.interaction';

import WarningAddInteraction from './command/warning/add.interaction';
import WarningListInteraction from './command/warning/list.interaction';
import WarningRemoveInteraction from './command/warning/remove.interaction';

import BanInteraction from './command/ban.interaction';
import UnbanInteraction from './command/unban.interaction';
import BanListInteraction from './command/ban-list.interaction';

import ConfigInteraction from './command/config.interaction';

import GoogleInteraction from './command/search/google.interaction';
import LMDDGTFYInteraction from './command/search/lmddgtfy.interaction';

import UserModInteraction from './command/user/mod.interaction';
import UserInfoInteraction from './command/user/info.interaction';

const interactions = {
  ping: PingInteraction,
  define: DefineInteraction,
  urban: UrbanInteraction,
  codeblocks: CodeBlockInteraction,
  config: ConfigInteraction,
  ban: BanInteraction,
  unban: UnbanInteraction,
  'ban-list': BanListInteraction,
  rule: {
    show: RuleShowInteraction,
    create: RuleCreateInteraction,
    edit: RuleEditInteraction,
    delete: RuleDeleteInteraction,
  },
  role: {
    assign: RoleAssignInteraction,
    delete: RoleDeleteInteraction,
    register: RoleRegisterInteraction,
    remove: RoleRemoveInteraction,
    show: RoleShowInteraction,
  },
  user: {
    mod: UserModInteraction,
    info: UserInfoInteraction,
  },
  warning: {
    add: WarningAddInteraction,
    list: WarningListInteraction,
    remove: WarningRemoveInteraction,
  },
  search: {
    google: GoogleInteraction,
    lmddgtfy: LMDDGTFYInteraction,
  },
};

export { interactions };
