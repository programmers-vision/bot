import { Client, Intents } from 'discord.js';

class ConfigService {
  public DEVS: string[] = [
    '756757056941326397',
    '299210434467069963',
    '546355035995373568',
  ];
  public joinRoles: { [guild: string]: string[] } = {};

  public INTENTS: number[] = [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_BANS,
    Intents.FLAGS.GUILD_MEMBERS,
    Intents.FLAGS.GUILD_MESSAGES,
    Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
    Intents.FLAGS.GUILD_EMOJIS_AND_STICKERS,
  ];

  async initialize(client: Client) {
    for (const guild of client.guilds.cache.values()) {
      const roles = guild.roles;
      for (const role of roles.cache.values()) {
        if (['Community Ping', 'Event Ping'].includes(role.name)) {
          if (this.joinRoles[guild.id] === undefined) {
            this.joinRoles[guild.id] = [role.id];
          } else {
            this.joinRoles[guild.id].push(role.id);
          }
        }
      }
    }
  }
}

const instance = new ConfigService();
export default instance;
