const nums = ['0️⃣', '1️⃣', '2️⃣', '3️⃣', '4️⃣', '5️⃣', '6️⃣', '7️⃣', '8️⃣', '9️⃣'];

class TextBuilder {
  text: string = '';

  addField(key: any, value?: any): TextBuilder {
    if (value) {
      this.text += `**${key}:** \`${value}\`\n`;
    } else {
      this.text += `**${key}:**\n`;
    }
    return this;
  }

  addEnumField(num: number, value: any): TextBuilder {
    this.text += `${num
      .toString()
      .split('')
      .map((i) => nums[i])
      .join('')} ${value}\n`;
    return this;
  }

  addItemField(key: any, value: any): TextBuilder {
    this.text += `❧ **${key}:** \`${value}\`\n`;
    return this;
  }

  appendLine(text: string): TextBuilder {
    this.text += text + '\n';
    return this;
  }

  build(): string {
    return this.text;
  }
}

export default TextBuilder;
