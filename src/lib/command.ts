import { Interaction } from 'discord.js';
import { Permission } from '#/bot';

export interface CommandExecutable {
  permission: Permission;

  execute(interaction: Interaction): Promise<void>;
}
