export const API_URL = 'https://api.dictionaryapi.dev/api/v2/entries/en/';

export interface WordDefinition {
  definition: string;
  example?: string;
  synonyms: Array<string>;
  antonyms: Array<string>;
}

export interface WordMeaning {
  partOfSpeech: string;
  definitions: Array<WordDefinition>;
}

export interface WordResult {
  word: string;
  phonetic: string;
  origin: string;
  meanings: Array<WordMeaning>;
}
