export const API_URL = 'https://api.urbandictionary.com/v0/define?term=';

// noinspection SpellCheckingInspection
export interface WordMeaning {
  permalink: string;
  author: string;
  word: string;
  definition: string;
  example: string;
  written_on: Date;
  sound_urls: string[];
  defid: number;
  current_vote: string;
  thumbs_up: number;
  thumbs_down: number;
}

export interface WordResult {
  list: Array<WordMeaning>;
}
