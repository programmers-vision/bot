declare global {
  namespace NodeJS {
    interface ProcessEnv {
      ENVIRONMENT?: string;
      LOKI_URL: string;
      BOT_HOST: string;
      METRICS_PORT: string;

      CLIENT_ID: string;
      DEV_GUILD?: string;
      TOKEN: string;
    }
  }
}

export {};
