-- CreateEnum
CREATE TYPE "permission" AS ENUM ('DEVELOPER', 'HEAD_ADMINISTRATOR', 'SENIOR_ADMINISTRATOR', 'ADMINISTRATOR', 'SENIOR_MODERATOR', 'MODERATOR', 'TRIAL_MODERATOR', 'VALUED_CONTRIBUTOR', 'USER', 'BOT', 'MUTED');

-- CreateEnum
CREATE TYPE "message_state" AS ENUM ('SENT', 'EDITED', 'DELETED');

-- CreateTable
CREATE TABLE "guild" (
    "id" CHAR(18) NOT NULL,
    "rules" TEXT[],

    CONSTRAINT "guild_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "guild_channel" (
    "id" CHAR(18) NOT NULL,
    "gid" CHAR(18) NOT NULL,
    "annotations" TEXT[],
    "log_channel" BOOLEAN NOT NULL DEFAULT false
);

-- CreateTable
CREATE TABLE "guild_member" (
    "technical_id" SERIAL NOT NULL,
    "id" CHAR(18) NOT NULL,
    "gid" CHAR(18) NOT NULL,
    "permission" "permission" NOT NULL DEFAULT E'USER',

    CONSTRAINT "guild_member_pkey" PRIMARY KEY ("technical_id")
);

-- CreateTable
CREATE TABLE "guild_role" (
    "id" SERIAL NOT NULL,
    "rid" CHAR(18) NOT NULL,
    "gid" CHAR(18) NOT NULL,
    "self" BOOLEAN NOT NULL DEFAULT false,

    CONSTRAINT "guild_role_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "message" (
    "gid" CHAR(18) NOT NULL,
    "cid" CHAR(18) NOT NULL,
    "uid" CHAR(18) NOT NULL,
    "id" CHAR(18) NOT NULL,
    "content" TEXT NOT NULL,
    "state" "message_state" NOT NULL DEFAULT E'SENT',
    "revision" INTEGER NOT NULL DEFAULT 0,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "edited_at" TIMESTAMP(3) NOT NULL,
    "deleted_at" TIMESTAMP(3)
);

-- CreateTable
CREATE TABLE "warning" (
    "wid" INTEGER NOT NULL,
    "gid" CHAR(18) NOT NULL,
    "uid" CHAR(18) NOT NULL,
    "active" BOOLEAN NOT NULL DEFAULT true,
    "mid" CHAR(18),
    "cid" CHAR(18),
    "rev" INTEGER DEFAULT 0,
    "reason" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- CreateTable
CREATE TABLE "ban" (
    "bid" INTEGER NOT NULL,
    "gid" CHAR(18) NOT NULL,
    "uid" CHAR(18) NOT NULL,
    "active" BOOLEAN NOT NULL DEFAULT true,
    "reason" TEXT NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "pardon_date" TIMESTAMP(3)
);

-- CreateTable
CREATE TABLE "_GuildMemberToGuildRole" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "guild_channel_id_gid_key" ON "guild_channel"("id", "gid");

-- CreateIndex
CREATE UNIQUE INDEX "guild_member_id_gid_key" ON "guild_member"("id", "gid");

-- CreateIndex
CREATE UNIQUE INDEX "guild_role_rid_gid_key" ON "guild_role"("rid", "gid");

-- CreateIndex
CREATE UNIQUE INDEX "message_gid_cid_uid_id_revision_key" ON "message"("gid", "cid", "uid", "id", "revision");

-- CreateIndex
CREATE UNIQUE INDEX "warning_wid_gid_uid_key" ON "warning"("wid", "gid", "uid");

-- CreateIndex
CREATE UNIQUE INDEX "warning_gid_cid_uid_mid_rev_key" ON "warning"("gid", "cid", "uid", "mid", "rev");

-- CreateIndex
CREATE UNIQUE INDEX "ban_bid_gid_uid_key" ON "ban"("bid", "gid", "uid");

-- CreateIndex
CREATE UNIQUE INDEX "_GuildMemberToGuildRole_AB_unique" ON "_GuildMemberToGuildRole"("A", "B");

-- CreateIndex
CREATE INDEX "_GuildMemberToGuildRole_B_index" ON "_GuildMemberToGuildRole"("B");

-- AddForeignKey
ALTER TABLE "guild_channel" ADD CONSTRAINT "guild_channel_gid_fkey" FOREIGN KEY ("gid") REFERENCES "guild"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "guild_member" ADD CONSTRAINT "guild_member_gid_fkey" FOREIGN KEY ("gid") REFERENCES "guild"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "guild_role" ADD CONSTRAINT "guild_role_gid_fkey" FOREIGN KEY ("gid") REFERENCES "guild"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "message" ADD CONSTRAINT "message_gid_fkey" FOREIGN KEY ("gid") REFERENCES "guild"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "message" ADD CONSTRAINT "message_cid_gid_fkey" FOREIGN KEY ("cid", "gid") REFERENCES "guild_channel"("id", "gid") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "message" ADD CONSTRAINT "message_uid_gid_fkey" FOREIGN KEY ("uid", "gid") REFERENCES "guild_member"("id", "gid") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "warning" ADD CONSTRAINT "warning_gid_fkey" FOREIGN KEY ("gid") REFERENCES "guild"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "warning" ADD CONSTRAINT "warning_uid_gid_fkey" FOREIGN KEY ("uid", "gid") REFERENCES "guild_member"("id", "gid") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "warning" ADD CONSTRAINT "warning_gid_cid_uid_mid_rev_fkey" FOREIGN KEY ("gid", "cid", "uid", "mid", "rev") REFERENCES "message"("gid", "cid", "uid", "id", "revision") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ban" ADD CONSTRAINT "ban_gid_fkey" FOREIGN KEY ("gid") REFERENCES "guild"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "ban" ADD CONSTRAINT "ban_uid_gid_fkey" FOREIGN KEY ("uid", "gid") REFERENCES "guild_member"("id", "gid") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_GuildMemberToGuildRole" ADD FOREIGN KEY ("A") REFERENCES "guild_member"("technical_id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_GuildMemberToGuildRole" ADD FOREIGN KEY ("B") REFERENCES "guild_role"("id") ON DELETE CASCADE ON UPDATE CASCADE;
